#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>

//���������� ��������� ������
struct DataStruct {
    unsigned long long key1;
    unsigned long long key2;
    std::string key3;
};
//������� ���������
bool cmp(DataStruct a, DataStruct b) {
    if (a.key1 == b.key1) {
        if (a.key2 == b.key2) {
            return a.key3 < b.key3;
        }
        return a.key2 < b.key2;
    }
    return a.key1 < b.key1;
}

std::string escape_quotes(std::string str) {
    std::string result;
    for (char c : str) {
        if (c == '"') result += "\"";
        else result += c;
    }
    return result;
}

std::ostream& operator<<(std::ostream& os, const DataStruct& data) {
    os << "(:key1 " << data.key1 << ":key2 0b" << std::hex << data.key2 << ":key3 \"" << escape_quotes(data.key3) << "\":)";
    return os;
}
std::istream& operator>>(std::istream& is, DataStruct& data) {
    char c;
    if (is >> std::ws >> c && c == '(') {
        std::string key;
        while (is >> std::ws >> c && c != ')') {
            if (c == ':') {
                is >> key;
                if (key == "key1") is >> std::ws >> data.key1;
                else if (key == "key2") {
                    std::string key2_str;
                    is >> std::ws >> key2_str;
                    if (key2_str.substr(0, 2) == "0b") {
                        std::stringstream ss;
                        ss << std::hex << key2_str.substr(2);
                        ss >> data.key2;
                    }
                }
                else if (key == "key3") {
                    std::getline(is >> std::ws, data.key3, '\"');
                }                
            }
        }
    }
    return is;
}




int main() {
    std::istringstream iss(" (:key1 10:key2 0b100:key3 \"Data1): \n (:key3 \"Data2:key2 0b11:key1 10:):)");
    std::vector<DataStruct> vec;
    DataStruct data;

    while (iss >> std::ws >> data) {
        vec.push_back(data);
    }

    std::sort(vec.begin(), vec.end(), cmp);

    std::copy(vec.begin(), vec.end(), std::ostream_iterator<DataStruct>(std::cout, "\n"));
    return 0;
}
